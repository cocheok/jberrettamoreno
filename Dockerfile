FROM nginx:alpine
COPY dist/jberrettamoreno-ui /usr/share/nginx/html
COPY config/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

# jberrettamoreno-ui

## Install
`npm install`

## Start
`npm start`

## Websocker communication

When the user opens the browser it stablish a websocket connection with jberrettamoreno-api and then the first message that receives is the report object that is a connections_by_hour report that has the count of connections per hour
`
[
    {
        "hour": 1559635200000,
        "count": 4
    },
    {
        "hour": 1559638800000,
        "count": 0
    },
    {
        "hour": 1559642400000,
        "count": 4
    }
]
`
All the hours comes in current mill format so the ui, based on the user location, displays the correspondant hour

Then, it receives a message per stablished connection and disconnection informing the number of users accesing at this moment, and with this information it updates the chart object stored.

## Api calls

When the user gets the detailed report, the application consumes the Rest API that jberrettamoreno-api offers, it gets the report list and searches the connections_by_ip report and then makes another call to the API to consume them

Get report List
`
GET http://jberrettamoreno.com/api/report/
`

Generate report
`
GET http://jberrettamoreno.com/api/report/5cf6209148c6450010108155/generate/today
`


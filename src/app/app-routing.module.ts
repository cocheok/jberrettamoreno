import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { cvComponent } from './cv/cv.component';
import { ReportComponent } from './report/report.component';

const routes: Routes = [
  { path: '', component: cvComponent },
  { path: 'cv', component: cvComponent },
  { path: 'report', component: ReportComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

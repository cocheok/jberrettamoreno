import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { of } from 'rxjs/internal/observable/of';


describe('AppComponent', () => {
  
  let appComponent: AppComponent;
  let mockSocketService;
  let mockLiveConnectionsService;
  let mockIpFyService;
  let mockStore;
  beforeEach(async(() => {
    

    mockSocketService = jasmine.createSpyObj(['initSocket', 'send', 'onMessage']);
    mockLiveConnectionsService = jasmine.createSpyObj(['changeMessage']);
    mockIpFyService = jasmine.createSpyObj(['getPublicIP']);
    mockStore = jasmine.createSpyObj(['dispatch']);

    appComponent = new AppComponent(mockSocketService, mockStore, mockLiveConnectionsService, mockIpFyService);
  }));
  it('should create AppComponent component', () => {
    expect(appComponent).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import { SocketService } from './services/socket.service';
import { Load, AddInfo } from './report/report.actions';
import { Store} from '@ngrx/store';
import { LiveConnectionsService } from './services/connectionsMessage.service';
import { IpFyService } from './services/ipfy.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'jberrettamoreno-ui';

  constructor(private socketService: SocketService, private store: Store<{ report: any }>, 
              private liveConnectionsService: LiveConnectionsService,
              private ipFyService: IpFyService
              ){
  }
  
  ngOnInit() {

    this.socketService.initSocket();

    this.ipFyService.getPublicIP().subscribe( publicIp => {
      this.socketService.send(publicIp);
    });
    this.socketService.onMessage('report')
      .subscribe((message) => {
        if(message.type === 'load'){
          this.store.dispatch(new Load(message.body));
        } else if(message.type === 'update'){
          if(message.body.action === 'connect'){
            this.store.dispatch(new AddInfo(message.body));
          }          
        } else if(message.type === 'count_connections'){
          this.liveConnectionsService.changeMessage(message.body);
        }
      });
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { cvComponent } from './cv/cv.component';
import { ReportComponent } from './report/report.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';  //<<<< import it here
import { NavigationComponent } from './navigation/navigation.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule, MatNativeDateModule, MatTableModule, MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatToolbarModule, MatTabsModule, MatSidenavModule, MatListModule, MatIconModule, MatTooltipModule, MatStepperModule, MatAutocompleteModule, MatSlideToggleModule, MatSelectModule, MatGridListModule} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ChartsModule } from 'ng4-charts/ng4-charts';
import { SocketService } from './services/socket.service';
import { StoreModule } from '@ngrx/store';
import { reportReducer } from './report/report.reducer';
import { LiveConnectionsService } from './services/connectionsMessage.service';
import { TableReportByIp } from './report/tables/reportByIp.component';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { IpFyService } from './services/ipfy.service';

@NgModule({
  declarations: [
    AppComponent,
    cvComponent,
    NavigationComponent,
    ReportComponent,
    TableReportByIp 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule, 
    MatButtonModule, 
    MatCheckboxModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule, 
    MatRippleModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatNativeDateModule,
    MatTooltipModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    ChartsModule,
    MatSelectModule,
    MatGridListModule,
    StoreModule.forRoot({ report: reportReducer }),
    HttpClientModule

  ],
  entryComponents: [],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatToolbarModule, 
    MatNativeDateModule, 
    MatSidenavModule, 
    MatListModule, 
    MatTooltipModule,
    MatStepperModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatSelectModule
  ],
  providers: [SocketService, LiveConnectionsService, ApiService, IpFyService],
  bootstrap: [AppComponent]
})
export class AppModule { }


export class Conference{    
    constructor(
        public title:string,
        public subtitle:string,
        public image:string,
        public description:string
    ){}
}
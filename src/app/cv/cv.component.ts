import { Component} from '@angular/core';
import { Technology } from './technology';
import { Conference } from './conference';
import { Job } from './job';
import {MatSnackBar} from '@angular/material';




@Component({
    selector: 'cv',
    templateUrl: './cv.component.html',
    styleUrls: ['./cv.component.css']
})
export class cvComponent{
    public technologies: Array<Technology>;
    public conferences: Array<Conference>;

    public accenureImagePath: string;
    public exoImagePath: string;

    durationInSeconds = 5;
    
    constructor(private snackBar: MatSnackBar){
       this.accenureImagePath = '/assets/images/accenture.png';
       this.exoImagePath = '/assets/images/exo.png';
    }

    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 2000,
      });
    }

    copyText(val: string){
      let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();


        if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {

          // save current contentEditable/readOnly status
          var editable = selBox.contentEditable;
          var readOnly = selBox.readOnly;
  
          // convert to editable with readonly to stop iOS keyboard opening
          selBox.contentEditable = 'true';
          selBox.readOnly = true;
  
          // create a selectable range
          var range = document.createRange();
          range.selectNodeContents(selBox);
  
          // select the range
          var selection = window.getSelection();
          selection.removeAllRanges();
          selection.addRange(range);
          selBox.setSelectionRange(0, 999999);
  
          // restore contentEditable/readOnly to original state
          selBox.contentEditable = editable;
          selBox.readOnly = readOnly;
      }
      else {
          selBox.select();
      }
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }

    ngOnInit(){
        this.technologies = [
          new Technology("Nodejs", "/assets/images/technologies/nodejs.png", ["mocha", "swagger", "socketio", "grunt", "express", "artillery", "node webkit"] ),
          new Technology("Angular", "/assets/images/technologies/angular.png", ["karma", "material", "bootstrap", "angular2", "angular7"] ),
          new Technology("Mongo", "/assets/images/technologies/mongo.png", ["mongoos", "replica set", "nosql"] ),
          new Technology("Postgre", "/assets/images/technologies/postgre.png", ["postgis", "sql"] ),
          new Technology("MySQL", "/assets/images/technologies/mysql.png", ["sql"] ),
          new Technology("MySQL", "/assets/images/technologies/sqlserver.png", ["sql", "Stored procedures"] ),
          new Technology("Docker", "/assets/images/technologies/docker.png", ["docker-compose", "Linux conteiners", "Windows conteiners", "Kubernetes"] ),
          new Technology("REDIS", "/assets/images/technologies/redis.png", ["nosql"] ),
          new Technology("NGINX", "/assets/images/technologies/nginx.png", ["ssl", "http", "server"] ),
          new Technology("Sawtooth", "/assets/images/technologies/sawtooth.png", ["blockchain"] ),
          new Technology("Elasticsearch", "/assets/images/technologies/elasticsearch.png", ["elasticsearch","logstash","kibana", "beats"] ),
          new Technology("VueJS", "/assets/images/technologies/vuejs.png", ["karma", "material", "bootstrap"] ),
          new Technology("Gitlab CI", "/assets/images/technologies/gitlab.png", ["continuous delivery", "continuous integration", "git", "github", "gitlab" ] ),
          new Technology("Ionic", "/assets/images/technologies/ionic.png", ["Ionic Pro", "mobile", "apps"] ),
          new Technology("Google Cloud Platform", "/assets/images/technologies/gcp.png", ["devops", "linux", "debian", "ubuntu"] ),
          new Technology("Amazon Web Services", "/assets/images/technologies/aws.png", ["devops", "linux", "debian", "ubuntu"] ),
          new Technology("Microsoft Azure", "/assets/images/technologies/azure.png", ["devops", "linux", "windows", "debian", "ubuntu"] ),
          new Technology("Java", "/assets/images/technologies/java.png", ["hibernate", "spring", "struts2"] ),
          new Technology("Python", "/assets/images/technologies/python.png", ["django", "bottle"] ),
          new Technology("C++", "/assets/images/technologies/cpp.png", ["sodium", "boostasio"] ),
          new Technology("Perl", "/assets/images/technologies/perl.png", ["cpan", "moose"] ),
          new Technology("C#", "/assets/images/technologies/csharp.png", ["dotnet core 2"] ),
          new Technology("PHP", "/assets/images/technologies/php.png", ["cake"] ),
          new Technology("ROS", "/assets/images/technologies/ros.png", ["robotics"] ),
          new Technology("Gazebo", "/assets/images/technologies/gazebo.png", ["robotics", "urdf"] ),
          new Technology("Rviz", "/assets/images/technologies/rviz.png", ["robotics", "urdf"] ),
          new Technology("Raspbian", "/assets/images/technologies/raspbian.png", ["robotics", "linux"] ),
          new Technology("Node-Red", "/assets/images/technologies/nodered.png", ["robotics", "linux", "iot"] ),
          new Technology("Arduino", "/assets/images/technologies/arduino.png", ["robotics"] ),
          new Technology("Blender", "/assets/images/technologies/blender.png", ["robotics", "meshes"] )
        ];
        this.conferences = [
            new Conference("OWASP Latam Tour 2016 - 22/04/2016", "\“Flujos de autenticación\” (Authentication flows)", "/assets/images/speaker/owasp.png", "I spoke about different kinds of authentication flows putting focus on OAuth2.0 protocol showing its flow in detail. I gave some security advice at the time to implement the protocol. I explained how to avoid Cross Site Request Forgery and Covert Redirect attacks"),
            new Conference("Ekoparty security conference 2015 - 21/10/2015", "\"Como robarle WIFI a tu vecin@\" (How to crack WIFI passwords)", "/assets/images/speaker/ekoparty.png", "I spoke about how to make an auditory over WIFI networks showing some vulnerabilities. How we can make attacks 100% efficient on the same ones, and how to increase your bandwidth while having several networks."),
            new Conference("Flisol C.A.B.A. 2015 - 25/04/2015", "\"Como robarle WIFI a tu vecin@\" (How to crack WIFI passwords)", "/assets/images/speaker/flisol.png", "I spoke about how to make an auditory over WIFI networks showing some vulnerabilities. How we can make attacks 100% efficient on the same ones, and how to increase your bandwidth while having several networks."),
            new Conference("PHP Day 2014 - 31/05/2014", "MapServer", "/assets/images/speaker/phpday.png", "I spoke about how to set up your own cartography server and interact dynamically with the shapes of a website using MapServer framework, the one could be used with various technologies. The aim of the talk was to show that anyone could have maps on his website without the necessity of using Google Maps or other external map service.")
        ]; 
    }
  
  
}

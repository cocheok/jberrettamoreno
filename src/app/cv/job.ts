
export class Job{    
    constructor(
        public name: string,
        public image: string,
        public description: string,
        public experiences?: Job[]
    ){}
}

export class Technology{    
    constructor(
        public name:string,
        public image:string,
        public frameworks:Array<string>
        
    ){}
}
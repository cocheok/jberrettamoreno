import { CountByHourChart } from "./countByHourChart.component";

describe('CountByHourChart', () => {
    let hours = ['0', '1', '2', '3', '4', '5', '6','7', '8', '9','10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24' ];
    let dataPerHour = [10,20,40,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    it('should create CountByHourChart component and with the expected attributes', () => {
        let component = new CountByHourChart(hours, dataPerHour);
        expect(component).toBeTruthy();
        expect(component.labels.length).toBe(hours.length);
        expect(component.data.length).toBe(dataPerHour.length);
    });

  });
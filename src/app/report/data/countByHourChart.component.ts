export class CountByHourChart{    
    constructor(
        public labels: Array<string>,
        public data: Array<number>
    ){}
}
import { IpEventData, IpData } from "./ipEventData.component";

describe('IpEventData', () => {
    let eventsMock = [{start: 1560364693787, end: 1560364693787, duration: 15}]


    it('should create IpEventData component and with the expected attributes', () => {
        let component = new IpEventData(eventsMock[0].start, eventsMock[0].end, eventsMock[0].duration);
        expect(component).toBeTruthy();
        expect(component.start).toBe(eventsMock[0].start);
        expect(component.end).toBe(eventsMock[0].end);
        expect(component.duration).toBe(eventsMock[0].duration);
    });
    it('should create IpData component and with the expected attributes', () => {
        let component = new IpData('192.168.0.1', eventsMock);
        expect(component).toBeTruthy();
        expect(component.ip).toBe('192.168.0.1');
        expect(component.events.length).toBe(eventsMock.length);
        expect(component.events[0].start).toBe(eventsMock[0].start);
        expect(component.events[0].end).toBe(eventsMock[0].end);
        expect(component.events[0].duration).toBe(eventsMock[0].duration);
    });

  });
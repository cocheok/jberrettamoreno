export class IpEventData{    
    constructor(
        public start: number,
        public end: number,
        public duration: number,
    ){}
}
export class IpData{    
    constructor(
        public ip: string,
        public events: Array<IpEventData>
    ){}
}
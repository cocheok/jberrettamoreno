import { ReportCountData } from "./reportCountData.component";

describe('ReportCountData', () => {
    
    it('should create ReportCountData component and with the expected attributes', () => {
        let component = new ReportCountData(1560308400000, 10);
        expect(component).toBeTruthy();
        expect(component.hour).toBe(1560308400000);
        expect(component.count).toBe(10);
    });

  });
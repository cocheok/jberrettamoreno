export class ReportCountData{    
    constructor(
        public hour: number,
        public count: number
    ){}
}
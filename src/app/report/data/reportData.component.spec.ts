import { ReportData } from "./reportData.component";

describe('ReportData', () => {
    let countByhour = [{hour: 1560308400000, count: 10}];
    let liveReportData = [{ip: '192.168.0.1', timestamp: '1560359916279', secuence: 'XXXX', action: 'connect'}];
    let chart = {labels: ['0'], data: [10]};
    it('should create ReportData component and with the expected attributes', () => {
        let component = new ReportData(countByhour, liveReportData, chart);
        expect(component).toBeTruthy();
        expect(component.countByHour.length).toBe(countByhour.length);
        expect(component.countByHour[0].hour).toBe( countByhour[0].hour);
        expect(component.countByHour[0].count).toBe( countByhour[0].count);
        expect(component.liveReportData.length).toBe(liveReportData.length);
        expect(component.liveReportData[0].ip).toBe(liveReportData[0].ip);
        expect(component.liveReportData[0].timestamp).toBe(liveReportData[0].timestamp);
        expect(component.chart.labels[0]).toBe(chart.labels[0]);
        expect(component.chart.data[0]).toBe(chart.data[0]);
    });

  });
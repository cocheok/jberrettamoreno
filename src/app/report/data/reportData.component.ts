import { ReportCountData } from "./reportCountData.component";
import { ReportLiveData } from "./reportLiveData.component";
import { CountByHourChart } from "./countByHourChart.component";

export class ReportData{    
    constructor(
        public countByHour: Array<ReportCountData>,
        public liveReportData: Array<ReportLiveData>,
        public chart: CountByHourChart
    ){}
}
import { ReportElement } from "./reportElement.component";

describe('ReportElement', () => {
    let mockReport = {
        "_id": "5cf72447038b7a0011395c94",
        "name": "Connections by hour",
        "description": "Count of connections grouped by hour",
        "type": "connections_by_hour",
        "body": {
            "size": 0,
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "action": {
                                    "query": "connect"
                                }
                            }
                        },
                        {
                            "range": {
                                "timestamp": {
                                    "gte": "now-1d",
                                    "lte": "now",
                                    "format": "epoch_millis"
                                }
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "group_by_hour": {
                    "date_histogram": {
                        "field": "timestamp",
                        "interval": "hour"
                    }
                }
            }
        },
        "__v": 0,
        "filters": [
            "today"
        ]
    };
    it('should create ReportElement component and with the expected attributes', () => {
        let component = new ReportElement(mockReport._id, mockReport.name, mockReport.description, mockReport.type, mockReport.body);
        expect(component).toBeTruthy();
        expect(component._id).toBe(mockReport._id);
        expect(component.name).toBe(mockReport.name);
        expect(component.description).toBe(mockReport.description);
        expect(component.type).toBe(mockReport.type);
        expect(component.body.aggs.group_by_hour.date_histogram.field).toBe(mockReport.body.aggs.group_by_hour.date_histogram.field);
        expect(component.body.aggs.group_by_hour.date_histogram.interval).toBe(mockReport.body.aggs.group_by_hour.date_histogram.interval);

    });

  });
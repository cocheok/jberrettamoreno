export class ReportElement{    
    constructor(
        public _id: string,
        public name: string,
        public description: string,
        public type: string,
        public body: any
    ){}
}
import { ReportLiveData } from "./reportLiveData.component";

describe('ReportLiveData', () => {
    
    it('should create ReportLiveData component and with the expected attributes', () => {
        let component = new ReportLiveData('192.168.0.1', '1560359916279', 'XXXX', 'connect');
        expect(component).toBeTruthy();
        expect(component.ip).toBe('192.168.0.1');
        expect(component.timestamp).toBe('1560359916279');
        expect(component.secuence).toBe('XXXX');
        expect(component.action).toBe('connect');
    });

  });
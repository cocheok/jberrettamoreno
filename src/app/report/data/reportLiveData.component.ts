export class ReportLiveData{    
    constructor(
        public ip: string,
        public timestamp: string,
        public secuence: string,
        public action: string
    ){}
}
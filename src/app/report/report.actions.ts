import { Action } from '@ngrx/store';
import { ReportLiveData } from './data/reportLiveData.component';
import { ReportCountData } from './data/reportCountData.component';
 
export enum ActionTypes {
  AddInfo = '[Report Component] AddInfo',
  Load = '[Report Component] Load'

}
 
export class AddInfo implements Action {
  readonly type = ActionTypes.AddInfo;
  constructor(public message: ReportLiveData){};
}
export class Load implements Action {
  readonly type = ActionTypes.Load;
  constructor(public message: Array<ReportCountData>){};
} 

export type ActionsUnion = AddInfo | Load;
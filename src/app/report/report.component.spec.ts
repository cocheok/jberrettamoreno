import { async } from '@angular/core/testing';
import {ReportComponent } from './report.component';

describe('ReportComponent', () => {
  
  let reportComponent: ReportComponent;
  let mockStore;
  let mockLiveConnectionsService;
  let mockApiService;
  
  beforeEach(async(() => {
    
    mockStore = jasmine.createSpyObj(['dispatch', 'pipe']);
    mockLiveConnectionsService = jasmine.createSpyObj(['changeMessage']);
    mockApiService = jasmine.createSpyObj(['getReportList','generateReportByIP']);
    reportComponent = new ReportComponent(mockStore, mockLiveConnectionsService, mockApiService);
  }));
  it('should create ReportComponent component', () => {
    expect(reportComponent).toBeTruthy();
  });

  
});

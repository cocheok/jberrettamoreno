import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ReportData } from './data/reportData.component';
import { LiveConnectionsService } from '../services/connectionsMessage.service';
import { ApiService } from '../services/api.service';
import { IpData } from './data/ipEventData.component';


export interface Filter {
  value: string;
  viewValue: string;
}
export interface Transaction {
  item: string;
  cost: number;
}

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}
@Component({
    selector: 'report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.css']
  })
export class ReportComponent{

 // lineChart
 public lineChartData:Array<any> = [
  {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: 'People'}
];
public lineChartLabels:Array<any> = ['0', '1', '2', '3', '4', '5', '6','7', '8', '9','10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24' ];
public lineChartOptions:any = {
  responsive: true
};
public lineChartColors:Array<any> = [
  { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }
];
public lineChartLegend:boolean = true;
public lineChartType:string = 'line';
public connectionsQuantity = 0;

// events
public chartClicked(e:any):void {
  console.log(e);
}

public chartHovered(e:any):void {
  console.log(e);
}

  

  report$: Observable<ReportData>;

  constructor(private store: Store<{ report: ReportData }>, 
              private liveConnectionsService: LiveConnectionsService,
              private apiService: ApiService
               ) {
                this.report$ = this.store.pipe(select('report'));
  }

  ips:Array<IpData>;

  ngOnInit() {
    
    var _this = this;
    this.liveConnectionsService.currentMessage.subscribe({
      next(connectionsQuantity) {
        if(connectionsQuantity){
          _this.connectionsQuantity = connectionsQuantity.count;
        }
      
      }
    });

    this.report$.pipe().subscribe({
      next(reportValue) {
        
        if(reportValue){
          let _lineChartData:Array<any> = new Array(1);
          _lineChartData[0] = {data: new Array(reportValue.chart.data.length), label: 'People'};
          for (let j = 0; j < reportValue.chart.data.length; j++) {
            _lineChartData[0].data[j] = reportValue.chart.data[j];
          }

          _this.lineChartData = _lineChartData;

          let _lineChartLabels:Array<any> = new Array(reportValue.chart.labels.length);

          for (let j = 0; j < reportValue.chart.labels.length; j++) {
            _lineChartLabels[j] = reportValue.chart.labels[j];
          }
          
          _this.lineChartLabels = _lineChartLabels;

        }
        
        
      },
      error(msg) { console.log('Error Getting Location: ', msg); }
    });
   
  }

  generateReportByIP(){
    this.apiService.getReportList().subscribe( reportList => {
      reportList.forEach(reportElement => {
        if(reportElement.type === 'connections_by_ip'){
          this.apiService.generateReportByIP(reportElement._id, 'today').subscribe( data  => {
            this.ips = data;
          });
        } 
        
      });
    });
  }

    
}

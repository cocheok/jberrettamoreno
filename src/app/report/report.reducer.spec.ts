import { async } from '@angular/core/testing';
import { reportReducer } from './report.reducer';
import { ActionTypes, ActionsUnion, Load, AddInfo } from './report.actions';

describe('ReportReducer', () => {
 
  let mockState;
  let mockedLoadMessage;
  let mockedUpdateMessage;
  beforeEach(async(() => {
    mockState = {"countByHour":[{"hour":1560495600000,"count":2}],"liveReportData":[],"chart":{"labels":["6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","0","1","2","3","4","5"],"data":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0]}};
    mockedLoadMessage = [
      {
          "hour": 1560495600000,
          "count": 2
      }
    ];
    mockedUpdateMessage = {
      'ip': '192.168.0.1',
      'action': 'connect',
      'secuence': 'XXXXXXXXX',
      'timestamp': '1560495600000'
  };
    mockState = jasmine.createSpyObj(['dispatch', 'pipe']);
  }));
  it('should create the ReportData component when a load message comes', () => {
    let mockAction = new Load(mockedLoadMessage);
    let result = reportReducer(undefined, mockAction);
    expect(result.countByHour).toEqual([{hour: 1560495600000, count: 2}]);
    expect(result.chart.data.length).toBe(24);
    expect(result.chart.labels.length).toBe(24);
  });
  
});

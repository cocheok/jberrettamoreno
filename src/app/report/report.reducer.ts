import { ActionTypes, ActionsUnion } from './report.actions';
import { ReportData } from './data/reportData.component';
import { ReportCountData } from './data/reportCountData.component';
import { ReportLiveData } from './data/reportLiveData.component';
import { CountByHourChart } from './data/countByHourChart.component';

let reportLive: Array<ReportLiveData> = [];
let reportCountData: Array<ReportCountData> = [];
let chart: CountByHourChart = new CountByHourChart([], []);
export const report = new ReportData(reportCountData, reportLive, chart);

export function reportReducer(state = report, action: ActionsUnion) {
  switch (action.type) {
    case ActionTypes.Load:
      let newState = new ReportData([], [], new CountByHourChart([], []));  
      newState.liveReportData = [];
      newState.countByHour = action.message;
      let labels = new Array<string>();
      let data = new Array<number>();

      //complete 24h in labels
      let actualTime = new Date().getHours();
      let startTime = actualTime+1;
      for(let i = 0; i<24; i++){
        if(startTime+i<24){
          labels.push((startTime+i).toString());
        }else {
          labels.push(((startTime+i) - 24).toString());
        }
        data.push(0);
        
      }
      newState.countByHour.forEach(reportHour => {
        let hour = new Date(reportHour.hour).getHours();
        if(hour < actualTime){
          data[24 - actualTime + (hour-1)] = reportHour.count;  
        } else if(hour > actualTime){
          data[(hour - actualTime - 1 )] = reportHour.count;
        } else {
          data[23] = reportHour.count;
        }       
      });

      newState.chart = new CountByHourChart(labels, data);
      return newState;
 
    case ActionTypes.AddInfo:
      // copy state
      let countByHourUpdate: Array<ReportCountData> = [];
      state.countByHour.forEach(countData => {
        let newCountData = new ReportCountData(countData.hour, countData.count);
        countByHourUpdate.push(newCountData);
      });
      let reportLiveDataUpdate: Array<ReportLiveData> = [];
      state.liveReportData.forEach(liveData => {
        let newLiveData = new ReportLiveData(liveData.ip, liveData.timestamp, liveData.secuence, liveData.action);
        reportLiveDataUpdate.push(newLiveData);
      });
      let chartDataUpdate = [];
      state.chart.data.forEach(chartData => {
        chartDataUpdate.push(chartData);
      });
      let chartLabelsUpdate = [];
      state.chart.data.forEach(chartLabel => {
        chartLabelsUpdate.push(chartLabel);
      });
      let chartUpdate = new CountByHourChart(chartDataUpdate, chartLabelsUpdate);
      let stateUpdate = new ReportData(countByHourUpdate, reportLiveDataUpdate, chartUpdate);  

      //update chart
      stateUpdate.liveReportData.push(action.message);
      let dateTime = new Date(action.message.timestamp);
      
      let roundedHourDateTime = new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate(), dateTime.getHours(), 0, 0, 0).getTime(); 
      let hourExist = false;

      for(let i = 0; i < stateUpdate.countByHour.length; i++ ){
        if(stateUpdate.countByHour[i].hour === roundedHourDateTime){
          stateUpdate.countByHour[i].count += 1;
          hourExist = true;
        }
      }

      if(!hourExist){
        let newHour = new ReportCountData(roundedHourDateTime, 1);
        stateUpdate.countByHour.push(newHour);
      }

      
      let labelsUpdate = new Array<string>();
      let dataUpdate = new Array<number>();

      //complete 24h in labels
      let actualTimeUpdate = new Date().getHours();
      let startTimeUpdate = actualTimeUpdate+1;
      for(let i = 0; i<24; i++){
        if(startTimeUpdate+i<24){
          labelsUpdate.push((startTimeUpdate+i).toString());
        }else {
          labelsUpdate.push(((startTimeUpdate+i) - 24).toString());
        }
        dataUpdate.push(0);
        
      }

      stateUpdate.countByHour.forEach(reportHour => {
        let hour = new Date(reportHour.hour).getHours();
        if(hour < actualTimeUpdate){
          dataUpdate[24 - actualTimeUpdate + (hour-1)] = reportHour.count;  
        } else if(hour > actualTimeUpdate){
          dataUpdate[(hour - actualTimeUpdate - 1 )] = reportHour.count;
        } else {
          dataUpdate[23] = reportHour.count;
        }       
      });

      stateUpdate.chart = new CountByHourChart(labelsUpdate, dataUpdate);
      return stateUpdate;
 
  }

}
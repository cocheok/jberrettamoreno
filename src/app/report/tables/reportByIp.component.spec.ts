import { TableReportByIp } from "./reportByIp.component";

describe('TableReportByIp', () => {
    
    it('should create TableReportByIp component and with the expected headers', () => {
        let component = new TableReportByIp();
        expect(component).toBeTruthy();
        expect(component.displayedColumns.length).toBe(3);
        expect(component.displayedColumns[0]).toBe('start');
        expect(component.displayedColumns[1]).toBe('end');
        expect(component.displayedColumns[2]).toBe('duration');
    });

  });
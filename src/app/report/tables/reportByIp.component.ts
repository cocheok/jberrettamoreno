import {Component, Input, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { IpEventData } from '../data/ipEventData.component';


@Component({
  selector: 'table-report-by-ip',
  templateUrl: 'reportByIp.component.html',
  styleUrls: ['reportByIp.component.css'],
})
export class TableReportByIp {
  displayedColumns: string[] = ['start', 'end', 'duration'];
  @Input() dataIpEvents: Array<IpEventData>;
  sortedDataIpEvent: IpEventData[];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  constructor() {}
  ngOnInit() {
    this.sortedDataIpEvent = this.dataIpEvents.slice();
    this.dataSource = new MatTableDataSource(this.dataIpEvents);

    this.dataSource.sort = this.sort;
 
  }
 
}

import { ApiService } from "./api.service";
import { TestBed, inject, getTestBed } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import {
    XHRBackend
  } from '@angular/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../environments/environment';
 
describe('ApiService', () => {
   let apiService: ApiService
   let httpMock: HttpTestingController;
   let injector: TestBed;
   afterEach(() => {
    httpMock.verify();
  });
   beforeEach(() => {

    TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule
        ],
        providers: [
            { provide: environment.apiUrl, useValue: 'http://example.com' },
            ApiService,
            { provide: XHRBackend, useClass: MockBackend },
          ]
      });
      injector = getTestBed();
      apiService = injector.get(ApiService);
      httpMock = injector.get(HttpTestingController);
   });
   
    

    it('should be initialized', inject([ApiService], (apiService: ApiService) => {
        expect(apiService).toBeTruthy();
      }));

      it('should have a method getReportList that returns an Observable<Array<ReportElement>>',
        inject([ApiService], (apiService) =>  {
    
              const mockResponse = [
                {
                    "_id": "5cf469cc9db1c9619c6c8933",
                    "name": "Detailed connections",
                    "description": "Detailed connection events",
                    "type": "detailed",
                    "body": {
                        "size": 10000,
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "range": {
                                            "timestamp": {
                                                "gte": "now-1d",
                                                "lte": "now",
                                                "format": "epoch_millis"
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    },
                    "__v": 0,
                    "filters": [
                        "today"
                    ]
                },
                {
                    "_id": "5cf469cc9db1c9619c6c8934",
                    "name": "Connections by hour",
                    "description": "Count of connections grouped by hour",
                    "type": "connections_by_hour",
                    "body": {
                        "size": 0,
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "match": {
                                            "action": {
                                                "query": "connect"
                                            }
                                        }
                                    },
                                    {
                                        "range": {
                                            "timestamp": {
                                                "gte": "now-1d",
                                                "lte": "now",
                                                "format": "epoch_millis"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "aggs": {
                            "group_by_hour": {
                                "date_histogram": {
                                    "field": "timestamp",
                                    "interval": "hour"
                                }
                            }
                        }
                    },
                    "__v": 0,
                    "filters": [
                        "today"
                    ]
                },
                {
                    "_id": "5cf469cc9db1c9619c6c8935",
                    "name": "Connections by ip",
                    "description": "Detailed of connections grouped by ip",
                    "type": "connections_by_ip",
                    "body": {
                        "size": 0,
                        "query": {
                            "bool": {
                                "must": [
                                    {
                                        "range": {
                                            "timestamp": {
                                                "gte": "now-1d",
                                                "lte": "now",
                                                "format": "epoch_millis"
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        "aggs": {
                            "group_by_ip": {
                                "terms": {
                                    "field": "ip"
                                },
                                "aggs": {
                                    "group_by_secuence": {
                                        "terms": {
                                            "field": "secuence"
                                        },
                                        "aggs": {
                                            "disconnection_time": {
                                                "max": {
                                                    "field": "timestamp"
                                                }
                                            },
                                            "connection_time": {
                                                "min": {
                                                    "field": "timestamp"
                                                }
                                            },
                                            "duration_in_seconds": {
                                                "bucket_script": {
                                                    "buckets_path": {
                                                        "start_time": "connection_time",
                                                        "end_time": "disconnection_time"
                                                    },
                                                    "script": "(params.end_time - params.start_time)/1000"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "__v": 0,
                    "filters": [
                        "today"
                    ]
                }
            ];

              
              apiService.getReportList().subscribe((reports) => {
                expect(reports.length).toBe(3);
                expect(reports[0].type).toBe('detailed');
                expect(reports[1].type).toBe('connections_by_hour');
                expect(reports[2].type).toBe('connections_by_ip');
              });
              const req = httpMock.expectOne('http://localhost:3000/api/report');
              expect(req.request.method).toBe("GET");
              req.flush(mockResponse);
      
        })
      );

});
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IpData } from '../report/data/ipEventData.component';
import { tap } from 'rxjs/operators';
import { ReportElement } from '../report/data/reportElement.component';
import { environment } from '../../environments/environment';

//const SERVER_URL =  window.location.href + '/api';

@Injectable()
export class ApiService {
  
  constructor(private httpClient: HttpClient) {}
  reportByIPObservable : Observable<IpData[]>;


  public getReportList(){
    return this.httpClient.get<Array<ReportElement>>(environment.apiUrl+'/report');
  }
  public generateReportByIP(id: string, filter: string){
    return this.httpClient.get<Array<IpData>>(environment.apiUrl+'/report/'+ id +'/generate/'+filter);
  }
  
  
}
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject, fakeAsync, ComponentFixture } from '@angular/core/testing';
import { LiveConnectionsService } from './connectionsMessage.service';

describe('LiveConnectionsService', () => {
  let liveConnectionsService: LiveConnectionsService;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiveConnectionsService]
    });
    liveConnectionsService = TestBed.get(LiveConnectionsService);
  });

  it('should create the service', inject([LiveConnectionsService], (service: LiveConnectionsService) => {
    expect(service).toBeTruthy();
  }));
  it('should have a message to initialize the connection quantity in 0',
    inject([LiveConnectionsService], (liveConnectionsService) =>  { 
        liveConnectionsService.currentMessage.subscribe((message) => {
            expect(message.count).toBe(0);
        });        
    })
  );
  it('should have a method changeMessage updates the connection quantity',
    inject([LiveConnectionsService], (liveConnectionsService) =>  {
        liveConnectionsService.changeMessage({'count': 1});
        liveConnectionsService.currentMessage.subscribe((message) => {
            expect(message.count).toBe(1);
        });
    })
  );
});

import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { ConnectionsData } from "src/app/report/data/connectionsData.component";

@Injectable()
export class LiveConnectionsService{    
  
    private messageSource: BehaviorSubject<ConnectionsData> = new BehaviorSubject<ConnectionsData>(new ConnectionsData(0));
    currentMessage = this.messageSource.asObservable();
    constructor(){}

    
    changeMessage(message: any){
        this.messageSource.next(message);
    }
    
}
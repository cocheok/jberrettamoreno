import { IpFyService } from "./ipfy.service";
import { TestBed, inject, getTestBed } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import {
    XHRBackend
  } from '@angular/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
 
describe('IpFyService', () => {
   let ipFyService: IpFyService;
   let httpMock: HttpTestingController;
   let injector: TestBed;
   afterEach(() => {
    httpMock.verify();
  });
   beforeEach(() => {

    TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule
        ],
        providers: [
            IpFyService,
            { provide: XHRBackend, useClass: MockBackend },
          ]
      });
      injector = getTestBed();
      ipFyService = injector.get(IpFyService);
      httpMock = injector.get(HttpTestingController);
   });
   
    

    it('should be initialized', inject([IpFyService], (ipFyService: IpFyService) => {
        expect(ipFyService).toBeTruthy();
      }));

      it('should have a method getPublicIP that returns an the client public ip',
        inject([IpFyService], (ipFyService) =>  {
    
              const mockResponse = {ip:'190.210.239.55'};
              
              ipFyService.getPublicIP().subscribe((ipResponse) => {
                expect(ipResponse.ip).toBe('190.210.239.55');
              });
              const req = httpMock.expectOne('https://api.ipify.org/?format=json');
              expect(req.request.method).toBe("GET");
              req.flush(mockResponse);
        })
      );

});
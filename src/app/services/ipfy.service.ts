import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class IpFyService {
  
  constructor(private httpClient: HttpClient) {}


  public getPublicIP(){
    return this.httpClient.get<string>('https://api.ipify.org/?format=json');
  }
  
  
}
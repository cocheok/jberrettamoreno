/* tslint:disable:no-unused-variable */

import { TestBed, async, inject, fakeAsync, getTestBed } from '@angular/core/testing';
import { SocketService } from './socket.service';
import { SocketIO, Server } from 'mock-socket';
import { environment } from '../../environments/environment';

describe('SocketService', () => {
    /* setup for mock-socket in order to test socket.io */
    let socketService: SocketService;
    let injector: TestBed;
    let mockServer = new Server(environment.wsUrl+'/socket');
    mockServer.on('connection', socket => {
          socket.emit('report', { message:'test message 1'});
    });
  beforeEach(() => {
    
    TestBed.configureTestingModule({
      imports: [ ],
      providers: [SocketService]
    });
    injector = getTestBed();
    socketService = injector.get(SocketService);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
  });
  
  it('should create the service', inject([SocketService], (service: SocketService) => {
    expect(service).toBeTruthy();
  }));
  
  it('should receive a message on the report channel', async(done: DoneFn) => {
      (window as any).io = SocketIO;  
      // setting up client
      socketService.socket = io(environment.wsUrl+'/socket');
      
      socketService.onMessage('report').subscribe(message => {
        expect(message.message).toEqual('test message 1');
        mockServer.stop();
        done();
      });
  });
  
});


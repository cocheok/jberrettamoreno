import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import * as socketIo from 'socket.io-client';

@Injectable()
export class SocketService {
    private _socket;
    constructor() { }
    get socket(): any {
        return this._socket;
    }
    set socket(set:any) {
        this._socket = set;
    }
    public initSocket(): void {
        this._socket = socketIo(environment.wsUrl,
        {
          path: '/socket'
        });
    }

    public send(message: any): void {
        this._socket.emit('message', message);
    }

    public onMessage(channel): Observable<any> {
        return new Observable<any>(observer => {
            this._socket.on(channel, (data: any) => observer.next(data));
        });
    }

}

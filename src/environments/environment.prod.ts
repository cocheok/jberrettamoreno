export const environment = {
  production: true,
  apiUrl: 'http://'+window.location.hostname + (window.location.port?':' + window.location.port + '/api' : '/api'),
  wsUrl: window.location.hostname + (window.location.port?':' + window.location.port: '')
};
